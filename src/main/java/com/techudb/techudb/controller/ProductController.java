package com.techudb.techudb.controller;

import com.techudb.techudb.models.ProductModel;
import com.techudb.techudb.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/apitechu/v2")
public class ProductController {

    @Autowired
    ProductService productService;

    @PostMapping("/products")
    public ResponseEntity<ProductModel> addProducts(@RequestBody ProductModel product){

        System.out.println("addProducts");
        System.out.println(product.getId());
        ResponseEntity<ProductModel> response = new ResponseEntity<>(this.productService.add(product),
                HttpStatus.CREATED);
        return response;
    }

}
