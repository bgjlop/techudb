package com.techudb.techudb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TechudbApplication {

	public static void main(String[] args) {
		SpringApplication.run(TechudbApplication.class, args);
	}

}
