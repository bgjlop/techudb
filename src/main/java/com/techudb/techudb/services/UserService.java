package com.techudb.techudb.services;

import com.techudb.techudb.models.UserModel;
import com.techudb.techudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service

public class UserService {@Autowired
UserRepository userRepository;

    public UserModel add(UserModel user) {
        System.out.println("add en UserService");

        return this.userRepository.save(user);
    }

    public List<UserModel> findAll() {
        System.out.println("findAll UserService");

        return this.userRepository.findAll();

    }

    public Optional<UserModel> findById(String id) {
        System.out.println("El findById de UserService");

        return this.userRepository.findById(id);
    }

    public UserModel update(UserModel userModel) {
        System.out.println("update en UserModel");

        return this.userRepository.save(userModel);
    }

    public boolean delete(String id){
        System.out.println("delete en UserService");
        boolean result = false;

        if (this.findById(id).isPresent() == true) {
            System.out.println("Usuario encontrado, borrando");
            this.userRepository.deleteById(id);
            result = true;
        }

        return result;
    }

    public List<UserModel> getUsers(String orderBy) {
        System.out.println("getUsers en UserService");

        List<UserModel> result;

        if (orderBy != null) {
            System.out.println("Se ha pedido ordenacion");

            result = this.userRepository.findAll(Sort.by("age"));
        } else {
            result = this.userRepository.findAll(Sort.by("age"));
        }

        return result;
    }
}
