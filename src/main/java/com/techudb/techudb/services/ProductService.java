package com.techudb.techudb.services;

import com.techudb.techudb.models.ProductModel;
import com.techudb.techudb.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    public ProductModel add(ProductModel product){
        System.out.println("add ProductService");
        return this.productRepository.save(product);
    }
}
